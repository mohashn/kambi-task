import os
from datetime import datetime, timedelta

from jose import JWTError, jwt
from fastapi import Depends, FastAPI, HTTPException, status
from fastapi.security import HTTPBearer

from src.models import schemas


SECRET_KEY = os.getenv(
    'SECRET_KEY', "13aa2b3f2664089ad9d7cd195ae4ac5d6e42f0c282468be0215d970ecca0fdc9")
ALGORITHM = os.getenv("ALGORITHM", "HS256")
ACCESS_TOKEN_EXPIRE_DAYS = os.getenv("ACCESS_TOKEN_EXPIRE_DAYS", 365)
ACCESS_TOKEN_EXPIRE_DAYS = int(ACCESS_TOKEN_EXPIRE_DAYS)

""" we could use this token for test:
    eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJrYW1iaS10YXNrIiwiZX\
        hwIjoxNjQyOTk4MTYzfQ.AOslHukXQFALlua3nHKbxetxJCjsXP3VlvUt63dWi0Y
"""

security = HTTPBearer()


def parse_token(token):
    try:
        payload = jwt.decode(
            token.credentials, SECRET_KEY,algorithms=[ALGORITHM])
        user = payload.get("sub")
        return user
    except JWTError:
        return None


def verified(token=Depends(security)):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    if parse_token(token) == 'kambi-task':
        return True
    else:
        raise credentials_exception
