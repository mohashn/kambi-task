from datetime import datetime
from enum import Enum
from typing import List, Dict, Any

from pydantic import BaseModel, Field


class Command(BaseModel):
    name: str = Field(...)
    parameters: List[str] = Field(...)


class CommandResponse(BaseModel):
    result: str
    detail: str


class LSCommand(BaseModel):
    folder_name: str = Field(...)
    parameters: List[str] = Field(...)


class ResponseResult(str, Enum):
    OK = "ok"
    ERROR = "error"


class ListFolderResponse(BaseModel):
    path: str
    count: int
    items: List[str]
    result: ResponseResult
    description: str


class CommandNotFoundError(BaseModel):
    type: str = "ValueError.not_found"
    msg: str = "COMMAND OR FOLDER NOT FOUND"
    loc: List[str] = [""]


class FolderNotFoundError(BaseModel):
    type: str = "ValueError.folder_not_found"
    msg: str = "FOLDER NOT FOUND"
    loc: List[str] = [""]
