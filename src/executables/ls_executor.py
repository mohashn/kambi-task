import time
import subprocess

from src.models.schemas import ListFolderResponse




def find_folder(folder_name):
    process = subprocess.Popen(
        ['find', '..', '-name', folder_name],
        stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = process.communicate()
    return stdout, stderr

def get_list(path, params):
    ls_command = ['ls',path]
    ls_command.extend(params)
    process = subprocess.run(
        ls_command, stdout=subprocess.PIPE, 
        stderr=subprocess.PIPE, universal_newlines=True)
    return process.stdout, process.stderr

def parse_result(results):
    response = []
    for path, value in results.items():
        items = value[0].strip().split("\n")
        if items[0]:
            items.remove('.')
            items.remove('..')
        result = 'ok'
        description = ''
        count = len(items)
        if count == 0:
            description = 'Empty Folder'
        if value[1]:
            result = "error"
            description = value[1]
            count = 0
        response.append(ListFolderResponse(
            path=path,
            count = count,
            items = items,
            result = result,
            description = description
        ))   
    return response

def run_ls(command_params):
    folder_name = command_params.folder_name
    params = command_params.parameters
    """Check if folder exist for example in project folder
    """
    stdout, _ = find_folder(folder_name)
    paths = []
    if stdout:
        paths = stdout.decode('utf8').strip().split('\n')
    
    """ =Get list of 
    """
    results = {}
    for path in paths:
        results[path] = (get_list(path, params))
    response = parse_result(results)
    return response

