import subprocess

from src.models.schemas import ListFolderResponse


class Command:
    def __init__(self, name, params=[]):
        self.name = name
        self.params = params

    def execute(self):
        command_with_params = [self.name]
        command_with_params.extend(self.params)
        try:
            process = subprocess.run(
                command_with_params, stdout=subprocess.PIPE, 
                stderr=subprocess.PIPE, universal_newlines=True)
            return process.stdout, process.stderr
        except FileNotFoundError:
            return None, "FileNotFoundError"

    def help(self):
        command_with_params = [self.name]
        command_with_params.extend(['--help'])
        try:
            process = subprocess.run(
                command_with_params, stdout=subprocess.PIPE,
                stderr=subprocess.PIPE, universal_newlines=True)

            return process.stdout.split("\n"), process.stderr.split("\n")
        except FileNotFoundError:
            return None, None
