import time
import sys
import asyncio
from typing import List
from concurrent.futures import ThreadPoolExecutor

from fastapi import FastAPI, HTTPException, Response, status, Request, Depends
import uvicorn
from loguru import logger
from starlette.routing import Match

from src.models import schemas
from src.executables.general_executor import Command
from src.executables.ls_executor import run_ls
from src.authentication.jwt_auth import verified

tags_metadata = [
    {
        "name": "Main Task",
        "description": "Executing in general",
    },
    {
        "name": "Examples",
        "description": "design models by assuming 'ls' is command",
    },
]

logger.remove()
logger.add(sys.stdout, colorize=True, format="<green>{time:HH:mm:ss}</green> | {level} | <level>{message}</level>")
logger.add("logs.log")
app = FastAPI()


@app.on_event("shutdown")
def shutdown_event():
    # loop = asyncio.get_event_loop()
    # pending = asyncio.Task.all_tasks()
    ## Run loop until tasks done:
    # loop.run_until_complete(asyncio.gather(*pending))
    print("shutdown")


@app.middleware("http")
async def log_middle(request: Request, call_next):
    logger.debug(f"{request.method} {request.url}")
    routes = request.app.router.routes
    logger.debug("Params:")
    for route in routes:
        match, scope = route.matches(request)
        if match == Match.FULL:
            for name, value in scope["path_params"].items():
                logger.debug(f"\t{name}: {value}")
    logger.debug("Headers:")
    for name, value in request.headers.items():
        logger.debug(f"\t{name}: {value}")

    response = await call_next(request)
    return response

@app.middleware("http")
async def log_request(request: Request, call_next):
    logger.info(f'{request.method} {request.url}')
    response = await call_next(request)
    logger.info(f'Status code: {response.status_code}')
    body = b""
    async for chunk in response.body_iterator:
        body += chunk
    
    return Response(
        content=body,
        status_code=response.status_code,
        headers=dict(response.headers),
        media_type=response.media_type
    )

@app.middleware("http")
async def add_process_time_header(request: Request, call_next):
    start_time = time.time()
    response = await call_next(request)
    process_time = time.time() - start_time
    response.headers["X-Process-Time"] = str(process_time)
    return response

#####routes 


@app.get("/questions/{command_name}", tags=["Examples"])
async def get_help(command_name: str):
    """This end point will givce help about parameteres
    """
    my_command = Command(command_name)
    help_response = my_command.help()
    if any(help_response):
        return help_response
    else: 
        resp_error = schemas.CommandNotFoundError()
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail=resp_error.dict())
    

@app.post("/questions", response_model=schemas.CommandResponse, 
    status_code=status.HTTP_201_CREATED, tags=["Main Task"])
async def post_command(payload: schemas.Command):
    """This end point will execute command with a json payload
    for example:
        payload ={
        "name": "ping",
        "parameters": ["-c 20", "kambi.com"]
        }
    """
    my_command = Command(payload.name, payload.parameters)
    executor = ThreadPoolExecutor(max_workers=5)
    loop = asyncio.get_event_loop()
    stdout, stderr = await loop.run_in_executor(executor, my_command.execute)
    # await asyncio.sleep(5)
    if stderr:
        resp_error = schemas.CommandNotFoundError()
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail=resp_error.dict())
    else:
        response = schemas.CommandResponse(result="OK", detail=stdout)
    return response



@app.post("/questions/ls", response_model=List[schemas.ListFolderResponse],
     status_code=status.HTTP_201_CREATED, tags=["Examples"])
async def post_ls_command(
    command_params: schemas.LSCommand = {
        "folder_name": "test",
        "parameters": [
            "-a"
        ]
        }, token: str = Depends(verified)):
    """This end point will find folder and list items in each folder 
        as an example for design model in details 
    """
    ls_response = run_ls(command_params)
    if not ls_response:
        resp_error = schemas.FolderNotFoundError()
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail=resp_error.dict())
    return  ls_response


if __name__ == "__main__":
    uvicorn.run("main:app", host="localhost", port=8000)