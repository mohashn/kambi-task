
import os
import sys
import inspect

from fastapi.testclient import TestClient
from httpx import AsyncClient
import pytest

currentdir = os.path.dirname(os.path.abspath(
    inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)

os.chdir('../')

from src.main import app

client = TestClient(app)


def test_get_help():
    response = client.get("/questions/ls")
    assert response.status_code == 200
    assert response.json()[0][0] == 'Usage: ls [OPTION]... [FILE]...'


def test_get_help_bad_command():
    response = client.get("/questions/lsss")
    assert response.status_code == 404
    assert response.json() == {
        'detail': {
            'type': 'ValueError.not_found',
            'msg': 'COMMAND OR FOLDER NOT FOUND',
            'loc': ['']
            }
        }


def test_post_command():
    response = client.post(
        "/questions",
        json={
            "name": "ls",
            "parameters": [
                "./src/executables", "-a"
            ]
        },
    )
    assert response.status_code == 201
    assert response.json() == {
        "result":"OK",
        "detail": ".\n..\ngeneral_executor.py\nls_executor.py\n__pycache__\n"
        }


def test_post_bad_command():
    response = client.post(
        "/questions",
        json={
            "name": "lss",
            "parameters": [
                ".", "-a"
            ]
        },
    )
    assert response.status_code == 404
    assert response.json() == {
        "detail": {
            "type":"ValueError.not_found",
            "msg":"COMMAND OR FOLDER NOT FOUND","loc":[""]
            }
        }


@pytest.mark.asyncio
async def test_get_help_ac():
    async with AsyncClient(app=app, base_url="http://127.0.0.1:8000") as ac:
        response = await ac.get("/questions/ls")
    assert response.status_code == 200
    assert response.json()[0][0] == 'Usage: ls [OPTION]... [FILE]...'


def test_post_ls():
    token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJrYW1iaS10YXNrIiwiZXhwIjoxNjQyOTk4MTYzfQ.AOslHukXQFALlua3nHKbxetxJCjsXP3VlvUt63dWi0Y"
    response = client.post(
        "/questions/ls",
        json={
            "folder_name": "test",
            "parameters": [
                "-a"
            ]
            },
        headers={'Authorization': f'Bearer {token}'}
    )
    assert response.status_code == 201
    assert response.json()[0].get('description') == "Empty Folder"
    assert response.json()[1].get('description') == "ls: cannot open directory '../kambi-task/tests/ls_test_folder/test': Permission denied\n"
    assert response.json()[2].get('count') == 5

